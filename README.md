# Data Creation

This repo is used to create datasets around a base scenario for Surrogate models, uncertainty analysis and clustering.
The code generates folders containing data that was sampled from distributions defined in a config script. Creates
runners for SLURM and includes a data extractor for IAMC formatted results to simply collect the data that is of
interest for the user.

# Usage

## Installation

Install the repo from the SESIT Gitlab page

    ```bash
    git clone https://gitlab.com/SESIT/data-creation.git
    ```

Install a model from our Modelling Suite (e.g. COPPER)

        ```bash
        git clone https://gitlab.com/SESIT/copper.git
        ```

Setup a folder with the base scenario. As seen in the 'copper-common' folder in this repo.
This folder should also contain a template for the SLURM job, see copper-common/COPPER.sl
In said template you can use the following variables that the script will fill in from the config file:
- ACCOUNT (account name on cedar)
- EMAIL (email address for notifications)
- COMMAND (command to run the model)
- REPO_PATH (absolute path to the model repo)

Where ever you write SCENARIO in the template, the script will replace it with the scenario name.

## Configuration
For the script to work you need to create a config file. The config file should look like this:

```python

runner:
  slurm: <path to slurm script>
  command: <command to run the model>
  repo_path: <absolute path to the model repo>
  account: <account name on cedar>
  email: <email address for notifications>

sampler:
    name: <name of sampling function>
    kwargs:
        <kwargs for the sampling function>

"""
Define which files are of interest for the user (see copper_config.yaml as an example)
"""

```
You can use the constants SCENARIO and WORKINGPATH in the config file to refer to the scenario name and the working path of this repo respectively.

The general structure for the files is as follows:

General Structure
- **File Name**: The name of the file where the parameters are defined (e.g., config.toml, annual_growth.csv).
---
If the file is **.toml**:
  - **Category**: The key inside the config (e.g., Carbon, Hydro, Thermal).
    - **Sub-Category**
      - ...
        - min
        - max
        - peak

If the file is **.csv**:
- **index**: replaces the entire row using the column name to define which column should be used as the index and the row name as the index in set row to define the row.
  - \<column\>
    - \<row\>
      - min
      - max
      - peak
    
- **column**: replaces the entire column using the column name to define which column should be targeted.
  - \<column\> 
    - min
    - max
    - peak
     
- **cell**: replaces a single cell using the column name to define which column should be targeted and the row name as the index in said column to define the cell.
  - \<column\>
    - \<row\>
      - min
      - max
      - peak

---
- **Sample Method**: The method used for sampling the parameter values (e.g., replace, relative, interpolate, from_csv).
  - **Replace**: 
    - This method replaces the target values with the sampled values directly.
    - Example: If the sampled value is 5, all target values will be replaced with 5.
  - **Relative**: The parameter values are multiplied by the sampled values.
    - This method scales the target values by a relative percentage based on the sampled value.
    - Example: If the sampled value is 10, the target values will be scaled by 1.1 (i.e., 1 + 10/100).
  - **Interpolate**:
    - This method interpolates the target values between a start and end value based on the sampled percentage.
    - Example: If the sampled value is 20, the target values will be interpolated between 1 and 1.2 (i.e., 1 + 20/100).
  - **From_CSV** (only for CSV files): The parameter values are sampled from a CSV file.
    - **File**: The name of the CSV file from which the parameter values are sampled.
    - **Folder**: The folder where a CSV for each scenario is stored.

## Running the script
Once the data and config are defined you run the script with the following command:

```bash
  python config_reader.py --common_folder <path to the common folder> --config <path to the config file> --samples_output_folder <path where to output the scenarios> --shell_output_folder <path to output the shell scripts>
```

The script will create a folder for each scenario in the samples_output_folder and a shell script for each scenario in the shell_output_folder.
Additionaly it will create a file called shifted_samples.csv inside the the samples_output_folder. This file contains the sampled values for each scenario and variable.

Afterwards, copy the folders into the necessary location for the model to use them as input. E.g. COPPER uses the 'scenarios' folder in its repo.
Once the data is at the right location. Run:
    
```bash
bash run-queuer.sh <path to output the shell scripts>
```

This will submit all the jobs to SLURM and you can monitor the progress with the following command:

```bash
  squeue -u <your username>
```

Once the jobs are done you can extract data based on the reporting variables, by using the following command:

```bash
python data_extractor.py --path <regex defining the path to the results use * for wildcard> --output <path to output the data> --variables <List of variables to extract>
```