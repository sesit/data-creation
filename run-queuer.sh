#!/bin/bash

# Get the directory from the command line argument
DIR=$1

# Check whether the directory is provided and exists
if [ -z "$DIR" ]; then
  echo "Please provide a directory."
  exit 1
elif [ ! -d "$DIR" ]; then
  echo "$DIR is not a directory."
  exit 1
fi

# Find all bash scripts in the directory and submit them
for SCRIPT in $DIR/*.sh; do
  sbatch $SCRIPT
done
