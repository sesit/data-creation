import pandas as pd
import argparse
import glob
import re
import os




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Extract data from csv file')
    parser.add_argument('--path', type=str, help='Regex defining the paths to the csv files')
    parser.add_argument('--output', type=str, help='Output file path')
    # Use nargs='+' to accept a list of variables
    parser.add_argument('--variables', nargs='+', help='List of variables to extract')
    args = parser.parse_args()

    path = args.path
    output = args.output
    variables = args.variables

    print(f'Path: {path}')
    print(f'Output: {output}')
    print(f'Variables: {variables}')

    files = glob.glob(path)

    for file in files:
        print(f'Extracting data from {file}')
        # find sample number from path name
        sample_number = re.search(r'sample_\d+', file).group()

        data = pd.read_csv(file)
        data = data[data['variable'].str.startswith(tuple(variables))]

        # create output directory if it does not exist
        os.makedirs(output, exist_ok=True)

        data.to_csv(f'{output}/sample_{sample_number}.csv', index=False)



