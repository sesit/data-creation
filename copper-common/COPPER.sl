#!/bin/bash
#SBATCH --account=ACCOUNT
#SBATCH --mail-user=EMAIL
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem=57000M
##SBATCH --time=00:10:00
##SBATCH --output SCENARIO.out

echo ""
echo "Job Array ID / Job ID: $SLURM_ARRAY_JOB_ID / $SLURM_JOB_ID"
echo "This is job $SLURM_ARRAY_TASK_ID out of $SLURM_ARRAY_TASK_COUNT jobs."
echo ""

module load python/3.10 StdEnv/2023 gcc/12.3 arrow/15.0.1
echo ""
virtualenv --no-download $SLURM_TMPDIR/env
source $SLURM_TMPDIR/env/bin/activate
echo ""

cd REPO_PATH || exit

echo "made env, now installing packages"
echo ""
pip install --no-index --upgrade pip
pip install --no-index -r requirements.txt
echo "installed packages, now running python script"
echo ""
export PATH=$PATH:/project/rrg-mcpher16/SHARED2/software/cplex/cplex/bin/x86-64_linux/

echo "Running python script"
srun COMMAND
wait
echo "Finished running python script"


