import pandas as pd
import yaml
import toml
import os
import shutil
import numpy as np
from functools import partial

from SALib.sample import morris as morris_sample
from SALib.sample import sobol as sobol_sample
from SALib.sample.latin import sample as latin_sample

import argparse


def read_config(config_fname):
    """
    Reads the configuration file and returns the parsed configuration.

    :param config_fname: The name of the configuration file to be read.
    :type config_fname: str
    :return: The parsed configuration as a dictionary.
    :rtype: dict
    """
    config = yaml.load(open(config_fname, 'r'), Loader=yaml.FullLoader)
    return config


def assign_value_to_nested_dict(data, keys, value):
    """
    Assigns a value to a nested dictionary using a list of keys.

    :param data: The nested dictionary to modify.
    :param keys: A list of keys to access the desired location in the dictionary.
    :param value: The value to assign to the location specified by the keys.
    :return: True if the value is successfully assigned, False otherwise.
    """
    try:
        for key in keys[:-1]:
            data = data.setdefault(key, {})
        last_key = keys[-1]

        last_key_list = [last_key]
        # if last key is a string which represents a list (i.e. [*]), convert it to a list
        if last_key.startswith('[') and last_key.endswith(']'):
            del data[last_key]
            last_key = last_key[1:-1].split(',')

            last_key_list = [k.strip(' ') for k in last_key]

        for key in last_key_list:
            data[key] = value
        return True
    except (KeyError, TypeError):
        return False


def read_toml_file(key, value, flattened_config, s_config, file_config, folder_config):
    """
    Reads a TOML file and converts it into flattened and structured configurations.

    :param key: The current key being processed.
    :param value: The current value being processed.
    :param flattened_config: A dictionary that stores the flattened configuration.
    :param s_config: A dictionary that stores the structured configuration.
    :return: None

    """
    if isinstance(value, dict):
        for k, v in value.items():
            if k == 'sample':
                if key == '':
                    flattened_key = k
                else:
                    flattened_key = f'{key}|{k}'
                s_config[flattened_key] = v
            elif k == 'file':
                if key == '':
                    flattened_key = k
                else:
                    flattened_key = f'{key}|{k}'
                file_config[flattened_key] = v
            elif k == 'folder':
                if key == '':
                    flattened_key = k
                else:
                    flattened_key = f'{key}|{k}'
                folder_config[flattened_key] = v
            elif isinstance(v, dict):
                if key == '':
                    flattened_key = k
                else:
                    flattened_key = f'{key}|{k}'
                if v.keys() == {'min', 'max', 'peak'}:
                    flattened_config[flattened_key] = v
                else:
                    read_toml_file(flattened_key, v, flattened_config, s_config, file_config, folder_config)
            else:
                raise ValueError(f'Value {v} not understood')
    else:
        raise ValueError(f'Value {value} not understood')


def create_problem(config):
    """
    :param config: A dictionary containing the configuration for creating a problem. Each key in the dictionary represents a variable name, and the corresponding value is another dictionary containing properties of the variable.

    :return: A tuple containing the problem and offset. The problem dictionary contains information about the created problem, including the number of variables, variable names, bounds, and distributions. The offset dictionary contains the offset values added to the variables in order to meet the minimum criteria.

    """
    offset = {}
    for k, v in config.items():
        if v['min'] < 0:
            offset[k] = -v['min']
            v['min'] += offset[k]
            v['max'] += offset[k]

    problem = {
        'num_vars': len(config),
        'names': list(config.keys()),
        'bounds': [(v['min'], v['max'], v['peak']) for v in config.values()],
        'dists': ['triang' for _ in config.values()]
    }
    return problem, offset


def modify_toml_file(value, target, sample_idx, sample_method):
    """

    Parameters:
        value (dict or other): The value to be modified. If it is a dictionary, it will be recursively processed.
        target (dict or other): The target value to be modified. Must have the same structure as value.
        sample_idx (int): The index of the sample to be used for modifying the target.

    Returns:
        None

    Raises:
        ValueError: If the value or any of its sub-values is not understood.

    """
    if isinstance(value, dict):
        # make a dict sample_values that is the same as value, but with out the sample key
        sample_values = value.copy()
        if 'sample' in value.keys():
            sample_method = sample_values.get('sample', 'replace')
            sample_values.pop('sample')

        for k, v in sample_values.items():
            print(k, sample_method)
            if isinstance(v, dict):
                modify_toml_file(v, target[k], sample_idx, sample_method)
            elif isinstance(v, np.ndarray):
                if sample_method == 'replace':
                    base_value = target[k]
                    base_shape = 1
                    if isinstance(base_value, list):
                        base_shape = len(base_value)
                    target[k] = np.ones((base_shape)) * v[sample_idx]
                    if len(target[k]) == 1:
                        target[k] = float(target[k][0])
                elif sample_method == 'interpolate':
                    start = 1
                    end = 1 + (v[sample_idx] / 100)
                    base_value = target[k]
                    base_shape = 1
                    if isinstance(base_value, list):
                        base_shape = len(base_value)
                    target[k] = np.linspace(start, end, base_shape).tolist()
                    if len(target[k]) == 1:
                        target[k] = float(target[k][0])
                elif sample_method == 'relative':
                    print(v[sample_idx], 'RELATIVE',
                          np.asarray(np.asarray(target[k]) * (1 + (v[sample_idx] / 100))).tolist())
                    target[k] = np.asarray(np.asarray(target[k]) * (1 + (v[sample_idx] / 100))).tolist()
                    if isinstance(target[k], list):
                        if len(target[k]) == 1:
                            target[k] = float(target[k][0])
            else:
                raise ValueError(f'Value {v} not understood')
    else:
        raise ValueError(f'Value {value} not understood')


def modify_csv_file(config, target, sample_idx):
    """
    :param config: a dictionary containing the configuration parameters for modifying the CSV file
    :param target: a pandas DataFrame representing the target CSV file to be modified
    :param sample_idx: an integer representing the index of the desired sample
    :return: the modified pandas DataFrame representing the target CSV file

    This method modifies a CSV file based on the specified configuration parameters and the desired sample index.
    The modification is performed by replacing, interpolating, or scaling the values in the CSV file according to the
    configuration specifications.

    Example usage:
    >>> config = {
    ...     'index': {
    ...         'columnA': {
    ...             'rowA': [1, 2, 3],
    ...             'rowB': [4, 5, 6]
    ...         },
    ...         'columnB': {
    ...             'rowC': [7, 8, 9]
    ...         }
    ...     },
    ...     'sample': 'replace'
    ... }
    >>> target = pd.DataFrame({
    ...     'columnA': [1, 1, 1, 4, 4, 4],
    ...     'columnB': [7, 7, 7, 8, 8, 8]
    ... })
    >>> sample_idx = 0
    >>> modified_target = modify_csv_file(config, target, sample_idx)
    >>> print(modified_target)
       columnA  columnB
    0        1        7
    1        1        7
    2        1        7
    3        1        7
    4        1        7
    5        1        7
    """
    if 'index' in config.keys():
        col_dict = config['index']
        for col_name, rows in col_dict.items():
            for row_name, samples in rows.items():
                row_indices = target[target[col_name] == row_name].index
                sample_method = config.get('sample', 'replace')
                if sample_method == 'replace':
                    row_values = np.ones((len(row_indices), len(target.columns))) * samples[sample_idx]
                elif sample_method == 'interpolate':
                    start = 1
                    end = 1 + (samples[sample_idx] / 100)
                    row_values = np.linspace(start, end, len(target.columns))
                    row_values = np.tile(row_values, (len(row_indices), 1))
                elif sample_method == 'relative':
                    row_values = target.loc[row_indices].values * (1 + (samples[sample_idx] / 100))
                else:
                    raise ValueError(f'Sample method {sample_method} not understood')
                row_df = pd.DataFrame(row_values, columns=target.columns, index=row_indices)
                row_df[col_name] = row_name
                target.loc[row_indices] = row_df.copy()
    elif 'columns' in config.keys():
        for col_name, samples in config['columns'].items():
            col_shape = target[col_name].shape
            sample_method = config.get('sample', 'replace')
            if sample_method == 'replace':
                col_values = np.ones(col_shape) * samples[sample_idx]
            elif sample_method == 'interpolate':
                start = 1
                end = 1 + (samples[sample_idx] / 100)
                # flatten colshape
                flat_shape = np.prod(col_shape)
                col_values = np.linspace(start, end, flat_shape)
                col_values = col_values.reshape(col_shape)
            elif sample_method == 'relative':
                col_values = target[col_name].values * (1 + (samples[sample_idx] / 100))
            else:
                raise ValueError(f'Sample method {sample_method} not understood')
            target[col_name] = col_values
    elif 'all' in config.keys():
        samples = config['all']
        # multiply all cells that have a numerical value with samples[sample_idx]
        targets = target.map(np.isreal)
        sample_method = config.get('sample', 'replace')
        if sample_method == 'replace':
            target[targets] = np.ones(target[targets].shape) * samples[sample_idx]
        elif sample_method == 'interpolate':
            start = 1
            end = 1 + (samples[sample_idx] / 100)
            subset = target[targets]

            interp = np.linspace(start, end, len(subset.columns))
            interp = np.tile(interp, (len(subset), 1))
            target[targets] = interp

        elif sample_method == 'relative':
            target[targets] *= (1 + (samples[sample_idx] / 100))
        else:
            raise ValueError(f'Sample method {sample_method} not understood')
    elif 'cell' in config.keys():
        cell_dict = config['cell']
        for col_name, rows in cell_dict.items():
            for row_name, samples in rows.items():
                # index is where the row_name is found in the first column and col_name is found in the columns
                row_index = target[target[target.columns[0]] == row_name].index
                col_index = target.columns.get_loc(col_name)

                sample_method = config.get('sample', 'replace')
                if sample_method == 'replace':
                    cell_values = np.ones(1) * samples[sample_idx]
                elif sample_method == 'interpolate':
                    start = 1
                    end = 1 + (samples[sample_idx] / 100)
                    cell_values = np.linspace(start, end, 1)
                elif sample_method == 'relative':
                    cell_values = target.iloc[row_index, col_index].values * (1 + (samples[sample_idx] / 100))
                else:
                    raise ValueError(f'Sample method {sample_method} not understood')
                target.iloc[row_index, col_index] = cell_values

    elif 'from_csv' == config.get('sample', None):
        print('Reading from csv', config)
        if 'file' in config.keys():
            file_path = config['file']
            df = pd.read_csv(file_path)

            # make first column the index
            df.set_index(df.columns[0], inplace=True)
            target.set_index(target.columns[0], inplace=True)
            target.update(df)

            target.reset_index(inplace=True)

        elif 'folder' in config.keys():
            folder_path = config['folder']
            file_path = os.path.join(folder_path, f'sample_{sample_idx}.csv')
            if os.path.exists(file_path):
                df = pd.read_csv(file_path)

                # make first column the index
                df.set_index(df.columns[0], inplace=True)
                target.set_index(target.columns[0], inplace=True)
                target.update(df)

                target.reset_index(inplace=True)
            else:
                raise ValueError(f'{file_path} not found')

        else:
            raise ValueError('File/ Folder path not found in config')

    else:
        raise ValueError(f'Config {config} not understood')

    return target


def sample_from_config(config):
    """
    :param config: A dictionary containing the configuration for the sampler.
    :return: A tuple containing the modified configuration, the number of samples generated, and a DataFrame of shifted samples.

    The `sample_from_config` method takes in a `config` parameter which is a dictionary representing the configuration for the sampler. The method first extracts the necessary parameters from the `config` dictionary to determine the type of sampler and its corresponding keyword arguments.

    Based on the specified sampler name, the method selects the appropriate sample function using partial function application. The selected sample function is then used to generate samples based on the problem defined in the `config` dictionary.

    The generated samples are then processed by subtracting the corresponding offset values, based on the offset dictionary derived from the flat configuration. The subtracted samples are stored in a DataFrame called `shifted_samples`.

    Additional configuration parameters are added to the `sample_dict`. Then, for each key-value pair in `sample_dict`, the method splits the key by '|' delimiter and assigns the value to the nested dictionary in the `config`.

    Finally, the method returns the modified `config` dictionary, the number of samples generated, and the shifted samples DataFrame.
    """
    sampler_config = config['sampler']
    kwargs = sampler_config.get('kwargs', {})
    if sampler_config['name'] == 'morris':
        sample_func = partial(morris_sample.morris.sample, **kwargs)
    elif sampler_config['name'] == 'sobol':
        sample_func = partial(sobol_sample.sample, **kwargs)
    elif sampler_config['name'] == 'lhs':
        sample_func = partial(latin_sample, **kwargs)

    del config['sampler']

    flat_config = {}
    sample_config = {}
    file_config = {}
    folder_config = {}
    read_toml_file('', config, flat_config, sample_config, file_config, folder_config)

    problem, offset_dict = create_problem(flat_config)

    samples = sample_func(problem=problem)

    sample_dict = {k: v for k, v in zip(flat_config.keys(), samples.T)}

    # subtract offset from sample_dict
    for k, v in offset_dict.items():
        sample_dict[k] -= v

    shifted_samples = pd.DataFrame(sample_dict)

    # add sample_config to sample_dict
    for k, v in sample_config.items():
        sample_dict[k] = v

    # add file_config to sample_dict
    for k, v in file_config.items():
        sample_dict[k] = v

    # add folder_config to sample_dict
    for k, v in folder_config.items():
        sample_dict[k] = v

    for k, v in sample_dict.items():
        keys = k.split('|')
        assign_value_to_nested_dict(config, keys, v)

    return config, samples.shape[0], shifted_samples


def setup_runner(config, input_path, output_path, sample_idx):
    """
    :param config: a dictionary containing configuration settings
    :param input_path: the path to the directory containing the shell file
    :param output_path: the path to the directory where the modified shell file will be saved
    :param sample_idx: the index of the sample

    :return: None

    This method modifies a shell file by replacing certain placeholders with values from the configuration dictionary and saves the modified shell file to the specified output directory.
    """
    shell_path = config['slurm']
    command = config['command']

    with open(os.path.join(input_path, shell_path), 'rt') as shell_file:
        shell_file_lines = shell_file.read()

    # replace COMMAND with command
    shell_file_lines = shell_file_lines.replace('COMMAND', command)

    # replace SCENARIO with sample_{sample_idx}
    shell_file_lines = shell_file_lines.replace('SCENARIO', f'sample_{sample_idx}')

    # replace EMAIL with email
    shell_file_lines = shell_file_lines.replace('EMAIL', config['email'])

    # replace ACCOUNT with account
    shell_file_lines = shell_file_lines.replace('ACCOUNT', config['account'])

    # replace REPO_PATH with repo_path
    shell_file_lines = shell_file_lines.replace('REPO_PATH', config['repo_path'])

    # get abs path of this file
    abs_path = os.path.abspath(__file__)
    # remove file name
    abs_path = os.path.dirname(abs_path)

    # replace WORKINGPATH with abs path of this file
    shell_file_lines = shell_file_lines.replace('WORKINGPATH', abs_path)

    with open(os.path.join(output_path, f'sample_{sample_idx}.sh'), 'wt') as shell_file:
        shell_file.writelines(shell_file_lines)


def evaluate_config(config, common_folder):
    """
    Evaluate the configuration by processing the provided files.

    :param config: A dictionary representing the configuration.
    :param common_folder: The path to the folder containing common files.
    :return: A tuple containing the sample collection, modified configuration, and shifted samples.
    """
    common_files = os.listdir(common_folder)
    sample_collection = []

    config, num_samples, shifted_samples = sample_from_config(config)

    for i in range(num_samples):
        print(f'Processing sample {i + 1}/{num_samples}')
        evaluated_files = {}
        for key, value in config.items():
            if key in common_files:
                if 'toml' in key:
                    toml_file = toml.load(os.path.join(common_folder, key))
                    sample_toml = toml_file.copy()
                    modify_toml_file(value, sample_toml, i, 'replace')
                    evaluated_files[key] = sample_toml.copy()
                elif 'csv' in key:
                    df = pd.read_csv(os.path.join(common_folder, key))
                    df = modify_csv_file(value, df, i)
                    evaluated_files[key] = df.copy()

            else:
                raise ValueError(f'File {key} not found in common folder')

        sample_collection.append(evaluated_files.copy())

    return sample_collection, config, shifted_samples


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--common_folder', type=str, default='copper-common')
    parser.add_argument('--config', type=str, default='copper_config.yaml')
    parser.add_argument('--samples_output_folder', type=str, default='copper-output')
    parser.add_argument('--shell_output_folder', type=str, default='copper-output-shell')
    args = parser.parse_args()

    common_folder = args.common_folder
    config_file = args.config
    output_folder = args.samples_output_folder
    shell_output_folder = args.shell_output_folder

    if os.path.exists(output_folder):
        shutil.rmtree(output_folder)

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    if os.path.exists(shell_output_folder):
        shutil.rmtree(shell_output_folder)

    if not os.path.exists(shell_output_folder):
        os.makedirs(shell_output_folder)

    config = read_config(config_file)
    runner_config = config['runner']
    del config['runner']

    evaluated_files, sampled_config, shifted_samples = evaluate_config(config, common_folder)
    shifted_samples.to_csv(os.path.join(output_folder, 'shifted_samples.csv'), index=False)
    for i, sample in enumerate(evaluated_files):
        sample_folder = os.path.join(output_folder, f'sample_{i}')
        if not os.path.exists(sample_folder):
            os.makedirs(sample_folder)
        setup_runner(runner_config, common_folder, shell_output_folder, i)
        for fname, data in sample.items():
            if 'toml' in fname:
                with open(os.path.join(sample_folder, fname), 'w') as f:
                    toml.dump(data, f)
            elif 'csv' in fname:
                data.to_csv(os.path.join(sample_folder, fname), index=False)
            else:
                raise ValueError(f'File {fname} not understood')
